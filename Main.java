import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        // Задание 1
        System.out.println(SumFactorialsOfPrimes(5));

        // Задание 2
        var change = Vending(21, 50);
        change.forEach((key, value) -> System.out.println(key + " " + value));

        // Задание 3
        System.out.println(Lenses(new int[]{-1, 0, 1}));
    }

    // <summary> Задание 1 Сумма факториалов всех простых чисел, не превышающих X </summary>
    // <param name="primeX"> Число Х </param>
    // <returns> Сумма </returns>
    public static int SumFactorialsOfPrimes(int primeX) {
        int sum = 0;
        for (int i = 2; i <= primeX; i++) {
            if (isPrime(i)) {
                sum += getFactorial(i);
            }
        }
        return sum;
    }

    static int getFactorial(int x) {
        int result = 1;
        for (int i = 1; i <= x; i++) {
            result *= i;
        }
        return result;
    }

    static boolean isPrime(int x) {
        int n = (int) Math.sqrt(x);
        for (int i = 2; i <= n; i++) {
            if (x % i == 0) {
                return false;
            }
        }
        return true;
    }

    // <summary> Задание 2 ПО для вендингового аппарата </summary>
    // <param name="orderSum"> Сумма заказа </param>
    // <param name="clientSum"> Внесенная сумма клиентом </param>
    // <returns> Сдача в виде словаря { номинал : количество }</returns>
    public static HashMap<Integer, Integer> Vending(int orderSum, int clientSum) {
        HashMap<Integer, Integer> result = new HashMap();
        int[] banknotes = {1, 2, 5, 10, 50, 100, 200, 500, 1000, 2000, 5000};
        int dif = clientSum - orderSum;

        while (dif != 0 && dif > 0) {
            for (int i = banknotes.length - 1; i >= 0; i--) {
                if (dif / banknotes[i] > 0) {
                    int quantity = dif / banknotes[i];
                    dif -= quantity * banknotes[i];
                    result.put(banknotes[i], quantity);
                }
            }

        }
        return result;
    }


    // <summary> Задание 3 Заказы на линзы для Инопланетян </summary>
    // <param name="dioptries"> Значения диоптрий каждого Инопланетянина </param>
    // <returns> Количество пар линз </returns>
    public static int Lenses(int[] dioptries) {
        int quantity = 0;
        Arrays.sort(dioptries);
        for (int i = 0; i < dioptries.length; i++) {
            if (i == dioptries.length - 1) {
                return ++quantity;
            }
            if (Math.abs(dioptries[i] - dioptries[i + 1]) <= 2) {
                i += 1;
            }
            quantity += 1;
        }
        return quantity;
    }
}
